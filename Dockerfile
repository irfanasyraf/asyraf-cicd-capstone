FROM maven:latest AS build
WORKDIR /
COPY src /src
COPY pom.xml /
RUN mvn  clean package

FROM openjdk:alpine
COPY --from=build /target/mvnproj-1.0-SNAPSHOT.jar /mvnproj-1.0-SNAPSHOT.jar
CMD java -cp /mvnproj-1.0-SNAPSHOT.jar com.asyraf.App
